/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pi.datamodels;

/**
 *
 * @author lewis
 */
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ToMobileJson {

    private Integer Type;
    private String CompanyId;
    private String Remarks;
    private String Payee;
    private String MCCMN;
    private String Amount;
    private String Reference;
    private String PrimaryAccountNumber;

    public String getPayee() {
        return Payee;
    }

    public void setPayee(String Payee) {
        this.Payee = Payee;
    }

    public String getMCCMN() {
        return MCCMN;
    }

    public void setMCCMN(String MCCMN) {
        this.MCCMN = MCCMN;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String Amount) {
        this.Amount = Amount;
    }

    public String getReference() {
        return Reference;
    }

    public void setReference(String Reference) {
        this.Reference = Reference;
    }

    public String getPrimaryAccountNumber() {
        return PrimaryAccountNumber;
    }

    public void setPrimaryAccountNumber(String PrimaryAccountNumber) {
        this.PrimaryAccountNumber = PrimaryAccountNumber;
    }
    public void ToMobileJson() {

    }

    public Integer getType() {
        return Type;
    }

    public void setType(Integer Type) {
        this.Type = Type;
    }

    public String getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(String CompanyId) {
        this.CompanyId = CompanyId;
    }

    public String getRemarks() {
        return Remarks;
    }

    public void setRemarks(String Remarks) {
        this.Remarks = Remarks;
    }

    

    public String tojson() {
       
        JSONObject Ordelines = new JSONObject();

        

       

      
        Ordelines.put("Payee", this.Payee);
        Ordelines.put("MCCMNC", "63902");
        Ordelines.put("PrimaryAccountNumber", this.PrimaryAccountNumber);
        Ordelines.put("Amount", this.Amount);
        Ordelines.put("Reference",this.Reference);
        Ordelines.put("SystemTraceAuditNumber", "Use random number generator function");

        JSONObject obj = new JSONObject();
        obj.put("CompanyID", "Your Company ID");
        obj.put("Remarks", "Salary");
        obj.put("Type", 1);

        JSONArray arr = new JSONArray();

        arr.add(Ordelines);

        obj.put("Orderlines", arr);

       // System.out.println(obj.toJSONString());

        String json = obj.toString();

        return json;
    }

}
