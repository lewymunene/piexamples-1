/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pi.datamodels;

/**
 *
 * @author lewis
 */
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ToBusinessJson {

    private Integer Type;
    private String Remarks;
    private String Payee;
    
    private String PrimaryAccountNumber;
    private String Amount;
    private String Reference;
    private String SystemTraceAuditNumber;

    public Integer getType() {
        return Type;
    }

    public void setType(Integer Type) {
        this.Type = Type;
    }

    public String getRemarks() {
        return Remarks;
    }

    public void setRemarks(String Remarks) {
        this.Remarks = Remarks;
    }

    public String getPayee() {
        return Payee;
    }

    public void setPayee(String Payee) {
        this.Payee = Payee;
    }

   

    
    public String getPrimaryAccountNumber() {
        return PrimaryAccountNumber;
    }

    public void setPrimaryAccountNumber(String PrimaryAccountNumber) {
        this.PrimaryAccountNumber = PrimaryAccountNumber;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String Amount) {
        this.Amount = Amount;
    }

    public String getReference() {
        return Reference;
    }

    public void setReference(String Reference) {
        this.Reference = Reference;
    }

    public String getSystemTraceAuditNumber() {
        return SystemTraceAuditNumber;
    }

    public void setSystemTraceAuditNumber(String SystemTraceAuditNumber) {
        this.SystemTraceAuditNumber = SystemTraceAuditNumber;
    }

    public void ToMobileJson() {

    }
    

    public String tojson() {
        /**
         * return "DataObject [Type=" + Type + ", CopmanyID=" + CompanyId +
         * ",Remarks="+Remarks+", list=" + Ordelines + "]";
         *
         *
         */
        JSONObject Ordelines = new JSONObject();

        //ToMobileJson mob = new ToMobileJson();

       

      
      
        Ordelines.put("Payee", "KPLC");
        Ordelines.put("Type", "7");
        Ordelines.put("PrimaryAccountNumber", "888880");
        Ordelines.put("Amount", "200");
        Ordelines.put("Reference", this.Reference);
        Ordelines.put("SystemTraceAuditNumber", "Use random number generator function");

        JSONObject obj = new JSONObject();
        obj.put("CompanyID", "Your Company ID");
        obj.put("Remarks", "Salary");
        obj.put("Type", 2);

        JSONArray arr = new JSONArray();

        arr.add(Ordelines);

        obj.put("Orderlines", arr);

        //System.out.println(obj.toJSONString());

        String json = obj.toString();

        return json;
    }

}
